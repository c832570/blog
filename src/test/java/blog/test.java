package blog;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import blog.common.dao.DaoTool;
import blog.services.BlogService;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by cuizhibin on 2017/3/7.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class test {

    @Resource
    DaoTool daoTool;

    @Resource
    BlogService blogService;

    @Test
    @Transactional
    @Rollback(false)
    public void testt(){
//        Map<String,Object> map = blogService.getBlogList(1, true, 0, 10);
//        System.out.println(map);
        Map<String, Object> map= daoTool.findListAndCountByPage("from Blog WHERE ownerUserId =?", Map.class, 1, 0, 10, "count", "list");
        System.out.println(map);
    }

}
