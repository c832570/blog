package blog;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by cuizhibin on 2017/4/18.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class MailTest {
    @Autowired
    JavaMailSenderImpl sender;

    @Test
    public void test() {
        SimpleMailMessage smm = new SimpleMailMessage();
        // 设定邮件参数
        smm.setFrom(sender.getUsername());
        smm.setTo("776005958@qq.com");
        smm.setSubject("Hello world");
        smm.setText("Hello world via spring mail sender");

        sender.send(smm);

    }
}
