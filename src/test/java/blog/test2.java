package blog;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import blog.common.dao.DaoTool;
import blog.common.entity.UserInfo;

import java.sql.Timestamp;

/**
 * Created by cuizhibin on 2017/3/9.
 */
public class test2 {

    @Transactional
    public static void main(String[] args) {
        UserInfo userinfo = new UserInfo();
        userinfo.setRegTime(new Timestamp(System.currentTimeMillis()));
        userinfo.setState(1);
        userinfo.setIsBlackList(0);
        userinfo.setPassword("1");
        userinfo.setUsername("12");
        userinfo.setEmail("12");

        ApplicationContext ctx = new FileSystemXmlApplicationContext( "classpath:applicationContext.xml");
        DaoTool daoTool = (DaoTool) ctx.getBean("jdbcTool");

        daoTool.save(userinfo);
    }
}
