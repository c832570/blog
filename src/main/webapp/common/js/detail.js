$(function () {

    var html = template.compile(sources);
    $.post('', {}, function (data) {
        $("#right_articles").html(html(data));
    })
});

var sources = '<article class="ui segment">'
    + '<div class="article_meta">'
    + '<div class="article_title">'
    + '<h1>'
    + '<label href="{{title_href}}">{{title}}</label>'
    + '</h1>'
    + '</div>'
    + '<div class="article_info article_info_p">'
    + '<div class="article_info_category"><i class="book icon large"></i><a class="ui tag label large">{{category}}</a></div>'
    + '<div class="article_info_tags">'
    + '{{each list as value index}}'
    + '<i class="tags icon large"></i>{{index}} - {{value}}'
    + '{{/each}}'
    + '</div>'
    + '</div>'
    + '<div class="article_content">'
    + '<p>{{content}}</p>'
    + '</div>'
    + '</div>'
    + '<div class="article_meta_date">'
    + '<a class="grep" href="#"><i class="calendar outline icon"></i>{{time | dateFormat:"yyyy-MM-dd"}}</a>'
    + '</div>'
    + '</article>';

var ml = '<div id="toc" class="toc-article" style="display: block;">'
    + '<strong class="toc-title">文章目录</strong>'
    + '<ol class="toc">'
    + '<li class="toc-item toc-level-2"><a class="toc-link" href="#引入干扰"><span class="toc-number">1.</span> <span class="toc-text">引入干扰</span></a></li>'
    + '<li class="toc-item toc-level-2"><a class="toc-link" href="#模块封装"><span class="toc-number">2.</span> <span class="toc-text">模块封装</span></a></li>'
    + '</ol>'
    + '</div>';
