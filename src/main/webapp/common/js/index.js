$(function () {

    // var html = template.compile(sources);
    // $.post(basePath + '/blog/getBlogList',{all:1},function (data) {
    //     $("#right_articles").html(html(data));
    // })
});

var sources = '<article class="ui segment">'
    + '<div class="article_meta">'
    + '<div class="article_title">'
    + '<h1>'
    + '<a class="grep" href="{{title_href}}">{{title}}</a>'
    + '</h1>'
    + '</div>'
    + '<div class="article_content">'
    + '<p>{{content_preview}}</p>'
    + '</div>'
    + '<div class="article_info">'
    + '<div class="article_info_category">'
    + '<i class="book icon large"></i>'
    + '<a class="grep" class="ui tag label large">{{category}}</a>'
    + '</div>'
    + '<div class="article_info_tags">'
    + '{{each list as value index}}'
    + '<i class="tags icon large"></i>{{index}} - {{value}}'
    + '{{/each}}'
    + '</div>'
    + '</div>'
    + '</div>'
    + '<div class="article_meta_date">'
    + '<a href="#"><i class="calendar outline icon"></i>{{time | dateFormat:"yyyy-MM-dd"}}</a>'
    + '</div>'
    + '</article>';

