
$(function () {
    $(document).on('click','.loading',function(){
        return;
    });
    loginModal.init();
    loginModal.initRule();

    //初始化tooltip，semantic ui最不科学的一点
    $('[data-content]').each(function () {
        $(this).popup();
    });
});


var loginModal = {
    init: function () {
        $('.signInModal').modal({
            onShow: function () {
                $('#l_form').form('clear');
            }
        }).modal('attach events', '.signUpModal .signIn').modal('attach events', '.lpdModal .signIn');
        $('.signUpModal').modal('attach events', '.signInModal .signUp').modal({
            onShow: function () {
                $('#r_form').form('clear')
            }
        });
        $('.lpdModal').modal('attach events', '.signInModal .pd a').modal({
            onShow: function () {
                $('#f_form').form('clear')
            }
        });
    },
    initRule: function () {
        $('#l_form').form({
            inline: true,
            fields: {
                l_uname: {
                    identifier: 'l_uname',
                    rules: [
                        {
                            type: 'minLength[3]',
                            prompt: '请输入用户名/UID/Email'
                        }
                    ]
                },
                l_pddd: {                   //无所谓
                    identifier: 'l_pd',     //和name一致
                    rules: [
                        {
                            type: 'regExp[/^(?![a-zA-z]+$)(?!0-9+$)(?![!@#$%^&*]+$)[a-zA-Z0-9!@#$%^&*]{6,}$/]',
                            prompt: '密码格式不正确（必须包含字母数字特殊字符中的任意两种）'
                        }
                    ]
                }
            }
        });
        $('#r_form').form({
            inline: true,
            fields: {
                r_uname: {
                    identifier: 'r_uname',
                    rules: [
                        {
                            type: 'minLength[3]',
                            prompt: '请输入用户名/UID/Email'
                        }
                    ]
                },
                r_pd: {                   //无所谓
                    identifier: 'r_pd',     //和name一致
                    rules: [
                        {
                            type: 'regExp[/^(?![a-zA-z]+$)(?!0-9+$)(?![!@#$%^&*]+$)[a-zA-Z0-9!@#$%^&*]{6,}$/]',
                            prompt: '密码格式不正确（必须包含字母数字特殊字符中的任意两种）'
                        }
                    ]
                },
                r_email: {
                    identifier: 'r_email',
                    rules: [
                        {
                            type: 'email',
                            prompt: '邮箱格式不正确'
                        }
                    ]
                }
            }
        });
        $('#f_form').form({
            inline: true,
            fields: {
                r_email: {
                    identifier: 'f_email',
                    rules: [
                        {
                            type: 'email',
                            prompt: '邮箱格式不正确'
                        }
                    ]
                }
            }
        });
    }
};


function login() {
    var $form = $('#l_form');
    $form.form('validate form');
    if (!$form.form('is valid')) {
        return;
    }
    var $btn = $('.signInModal .submit');
    $btn.addClass('loading');
    var username = $('[name=l_uname]').val();
    var password = $('[name=l_pd]').val();

    password = hex_md5(password);

    $.post('/user/login', {username: username, password: password}, function (data) {
        if (data.success) {
            window.location.reload();
        } else {
            $btn.removeClass('loading');
        }
    },'json');
}

function logout() {

    $.post('/user/logout', {}, function () {
        window.location.reload();
    },'json');
}

function reg() {

    var $form = $('#r_form');
    $form.form('validate form');
    if (! $form.form('is valid')) {
        return;
    }
    var $btn = $('.signUpModal .submit');
    $btn.addClass('loading');

    var userInfo = {
            'userInfo.username': $('[name=r_uname]').val(),
            'userInfo.password': hex_md5($('[name=r_pd]').val()),
            'userInfo.email': $('[name=r_email]').val()
        };

    $.post('/user/reg', userInfo, function (data) {
        if (data.success) {
            window.location.reload();
        } else {
            $btn.removeClass('loading');
        }
    },'json');
}

function fpd() {
    var $form = $('#f_email');
    $form.form('validate form');
    if (! $form.form('is valid')){
        return;
    }
    var $btn = $('.lpdModal .submit');
    $btn.addClass('loading');

    var userInfo = {
        email:$('[name=r_email]').val()
    };

    $.post('/user/fPd', {userInfo: userInfo}, function (data) {
        if (data.success) {
            //已发送邮件到邮箱
        } else {
            $btn.removeClass('loading');
        }
    },'json');
}
