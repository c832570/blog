<%@ page import="blog.common.utils.StaticUtils" %>
<%@ page import="blog.common.entity.UserInfo" %><%--
  Created by IntelliJ IDEA.
  User: huise
  Date: 2017/3/14
  Time: 15:31
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" %>

<jsp:include page="include.jsp"/>
<%--<script src="${pageContext.request.contextPath}/common/js/left.js"></script>--%>
<aside>
    <div class="left_top"></div>
    <div class="left_inner">
        <header>
            <div class="avatar">
                <a href="${pageContext.request.contextPath}/">
                    <img class="ui small circular image" src="${pageContext.request.contextPath}/pic/getHeadPhoto">
                </a>
            </div>
            <div class="left_href">
                <div class="ui link list">
                    <router-link to="${pageContext.request.contextPath}/" class="active item">HOME</router-link>
                    <a href="${pageContext.request.contextPath}/category" class="item">CATEGORY</a>
                    <a href="${pageContext.request.contextPath}/tags" class="item">TAGS</a>
                    <a href="${pageContext.request.contextPath}/about" class="item">ABOUT</a>
                </div>
                <div class="ui horizontal list">
                    <div data-content="admin@832570.top" data-position="bottom center" class="item"><i class="circular inverted teal mail outline icon large"></i></div>
                    <a data-content="weibo" data-position="bottom center" href="http://weibo.com/3901576442/profile?topnav=1&wvr=6" class="item" target="_blank"><i
                            class="circular inverted teal weibo icon large"></i></a>
                </div>
                <div class="ui link list left_in_out">
                    <a class="item left_login" v-if="!<%=isLogin%>">Login</a>
                    <a class="item left_logout" v-else v-on="logout">Welcome,<br><%=userName%>,<br>Logout</a>
                </div>
            </div>
        </header>
    </div>
    <div class="toc hide">
        <div id="toc" class="toc-article">'
            <strong class="toc-title">文章目录</strong>
            <div class="ui link list toc">
                <a class="item" href="#引入干扰">1.引入干扰</a>
                <a class="item" href="#模块封装">2.模块封装</a>
            </div>
        </div>
        <input type="button" id="tocButton" value="隐藏目录" title="点击按钮隐藏或者显示文章目录">
    </div>
</aside>
<div>
    <%--登录--%>
    <div class="ui modal small signInModal">
        <i class="close icon"></i>
        <div class="header">
            SIGN IN OR SIGN UP
        </div>
        <div class="content">
            <div class="ui two column middle aligned very relaxed stackable grid">
                <div class="column">
                    <form id="l_form" class="ui form">
                        <div class="field">
                            <label>Username/Email</label>
                            <div class="ui left icon input">
                                <input name="l_uname" type="text" placeholder="Username/Email">
                                <i class="user icon"></i>
                            </div>
                        </div>
                        <div class="field pd">
                            <label>Password</label>
                            <a>Forgot password?</a>
                            <div class="ui left icon input">
                                <input name="l_pd" type="password">
                                <i class="lock icon"></i>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="ui vertical divider">
                    Or
                </div>
                <div class="center aligned column">
                    <div class="ui big green labeled icon button signUp">
                        <i class="signup icon"></i>
                        Sign up
                    </div>
                </div>
            </div>
        </div>
        <div class="actions">
            <button class="ui teal button" onclick="login();">Login</button>
        </div>
    </div>
    <%--注册--%>
    <div class="ui modal small signUpModal">
        <i class="close icon"></i>
        <div class="header">
            SIGN UP
        </div>
        <div class="content">
            <div class="ui two column middle aligned very relaxed stackable grid">
                <div class="column">
                    <form id="r_form" class="ui form" onsubmit="return false;">
                        <div class="field">
                            <label>Username</label>
                            <div class="ui left icon input">
                                <input name="r_uname" type="text" placeholder="Username">
                                <i class="user icon"></i>
                            </div>
                        </div>
                        <div class="field">
                            <label>Email</label>
                            <div class="ui left icon input">
                                <input name="r_email" type="text" placeholder="Email">
                                <i class="Mail icon"></i>
                            </div>
                        </div>
                        <div class="field">
                            <label>Password</label>
                            <div class="ui left icon input">
                                <input name="r_pd" type="password">
                                <i class="lock icon"></i>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="ui vertical divider">
                    Or
                </div>
                <div class="center aligned column">
                    <div class="ui big green labeled icon button signIn">
                        <i class="signup icon"></i>
                        Sign in
                    </div>
                </div>
            </div>

        </div>
        <div class="actions">
            <button class="ui teal button" onclick="reg();">Sign up</button>
        </div>
    </div>
    <%--找回密码--%>
    <div class="ui modal small lpdModal">
        <i class="close icon"></i>
        <div class="header">
            FIND PASSWORD
        </div>
        <div class="content">
            <div class="ui two column middle aligned very relaxed stackable grid">
                <div class="column">
                    <form id="f_form" class="ui form" onsubmit="return false;">
                        <div class="field">
                            <label>Email</label>
                            <div class="ui left icon input">
                                <input name="f_email" type="text" placeholder="Email">
                                <i class="user icon"></i>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="ui vertical divider">
                    Or
                </div>
                <div class="center aligned column">
                    <div class="ui big green labeled icon button signIn">
                        <i class="signup icon"></i>
                        Sign in
                    </div>
                </div>
            </div>

        </div>
        <div class="actions">
            <button class="ui teal button" onclick="fpd();">Find Password</button>
        </div>
    </div>
</div>