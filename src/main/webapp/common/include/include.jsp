<%@ page import="blog.common.utils.StaticUtils" %>
<%@ page import="blog.common.entity.UserInfo" %><%--
  Created by IntelliJ IDEA.
  User: huise
  Date: 2017/3/14
  Time: 15:31
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" %>
<%
    UserInfo userInfo = (UserInfo) session.getAttribute(StaticUtils.LOGIN_USER_SESSION_KEY);
    boolean isLogin = userInfo != null;
    String userName = isLogin ? userInfo.getUsername() : "";
%>

<script>
    var basePath = '${pageContext.request.contextPath}';
    var isLogin = <%=isLogin%>;
    var uname = '<%=userName%>';
</script>

<link rel="bookmark" type="image/x-icon" href="${pageContext.request.contextPath}/common/images/avatar.png"/>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/common/images/avatar.png">

<%--jquery--%>
<script src="${pageContext.request.contextPath}/common/frame/jquery-1.12.4.js"></script>

<%--semantic ui--%>
<link href="${pageContext.request.contextPath}/common/frame/semantic-ui/semantic.css" rel="stylesheet"/>
<script src="${pageContext.request.contextPath}/common/frame/semantic-ui/semantic.js"></script>

<%--Ueditor--%>
<link href="${pageContext.request.contextPath}/common/frame/editor.md/css/editormd.css" rel="stylesheet"/>
<script src="${pageContext.request.contextPath}/common/frame/editor.md/editormd.js"></script>

<%--md5 和 artTemplate--%>
<script src="${pageContext.request.contextPath}/common/frame/md5.js"></script>
<script src="${pageContext.request.contextPath}/common/frame/template.js"></script>

<%--Vue--%>
<%--<script src="${pageContext.request.contextPath}/common/frame/vue/vue.js"></script>--%>
<%--<script src="${pageContext.request.contextPath}/common/frame/vue-router/vue-router.js"></script>--%>

<%--公共css和js--%>
<link href="${pageContext.request.contextPath}/common/css/main.css" rel="stylesheet"/>
<script src="${pageContext.request.contextPath}/common/js/common.js"></script>

<%--webpack--%>
<link href="${pageContext.request.contextPath}/common/css/style.css" rel="stylesheet"/>
<script src="${pageContext.request.contextPath}/common/index.js"></script>



