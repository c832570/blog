<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 16/12/03
  Time: 22:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>久唯</title>
    <jsp:include page="/common/include/left.jsp"/>
</head>
<body>
<div class="right">
    <%--<div class="loading ui active inverted dimmer">--%>
    <%--<div class="ui large text loader">Loading</div>--%>
    <%--</div>--%>
    <div class="right_articles">
        <article class="ui segment">
            <div class="article_meta">
                <div class="article_title">
                    <h1>
                        <label href="#">标题</label>
                    </h1>
                </div>
                <div class="article_info article_info_p">
                    <div class="article_info_category"><i class="book icon large"></i><a class="ui tag label large">ddd</a></div>
                    <div class="article_info_tags"><i class="tags icon large"></i></div>
                </div>
                <div class="article_content">
                    <p>测试测试测试测试测试<a>ddddd</a><br>测试测试测试测试测试<br>测试测试测试测试测试<br>content<br>content</p>
                </div>
            </div>
            <div class="article_meta_date">
                <a class="grep" href="#"><i class="calendar outline icon"></i>2017-01-01</a>
            </div>
        </article>
        <article class="ui segment">

        </article>
    </div>
    <div class="right_anchor">
        <a href="#"><i class="angle up icon"></i></a>
        <a href="#comments"><i class="comments outline icon"></i></a>
        <a href="#footer"><i class="angle down icon"></i></a>
    </div>
    <div class="right_pagebar">
    </div>
    <div class="right_pv">
    </div>
</div>
<div style="display: none;" id="footer"></div>
</body>
<script src="${pageContext.request.contextPath}/common/js/detail.js"></script>
</html>
