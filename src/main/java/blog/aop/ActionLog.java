package blog.aop;

import java.lang.annotation.*;

/**
 * Created by cuizhibin on 2017/4/3.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ActionLog {
    String desc()  default "无描述信息";
}
