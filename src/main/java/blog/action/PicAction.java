package blog.action;

import blog.aop.ActionLog;
import blog.common.utils.SessionAction;
import blog.services.PicServices;
import org.apache.struts2.convention.annotation.*;
import org.springframework.context.annotation.Scope;

import javax.annotation.Resource;
import java.io.InputStream;

/**
 * Created by cuizhibin on 2017/4/3.
 */
@ParentPackage("base")
@Namespace("/pic")
@Scope("prototype")
@Results({
        @Result(name = "json", type = "json", params = {"root", "result"}),
        @Result(name = "index", location = "/index.jsp"),
        @Result(name = "picStream", type = "stream", params = {"contentType", "application/octet-stream",
        "inputName", "inputStream", "bufferSize", "4096"})
})
public class PicAction extends SessionAction{
    private InputStream inputStream;
    private Object result;

    @Resource
    private
    PicServices picServices;

    /**
     * 读取头像
     *
     * @return
     */
    @Action("getHeadPhoto")
    @ActionLog(desc = "获取头像")
    public String getHeadPhoto() {
        inputStream = picServices.getHeadPhoto(getUserInfoFsession().getId());
        return "picStream";
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
