package blog.action;

import blog.aop.ActionLog;
import org.apache.struts2.convention.annotation.*;
import org.springframework.context.annotation.Scope;
import blog.common.entity.*;
import blog.common.utils.SessionAction;
import blog.common.utils.StaticUtils;
import blog.services.BlogService;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by cuizhibin on 2017/3/18.
 */
@ParentPackage("base")
@Namespace("/blog")
@Scope("prototype")
@Results({
        @Result(name = "json", type = "json", params = {"root", "result"}),
        @Result(name = "index", location = "/index.jsp")
})
public class BlogAction extends SessionAction {

    private Object result;

    @Resource
    private
    BlogService blogService;

    /**
     * ---------------------------------查询----------------------------------------------------
     */
    @Action("getBlogList")
    @ActionLog(desc = "获取博客列表")
    public String getBlogList() {
        result = blogService.getBlogList(getUserInfoFsession().getId(), all == 1, getIsAdmin(), page, size);
        return "json";
    }

    @Action("getBlogListByc")
    @ActionLog(desc = "获取博客列表by分类")
    public String getBlogListByc() {
        result = blogService.getBlogListByCategory(getUserInfoFsession().getId(), getIsAdmin(), cid, page, size);
        return "json";
    }

    @Action("getBlogListByt")
    @ActionLog(desc = "获取博客列表by标签")
    public String getBlogListByt() {
        result = blogService.getBlogListByTag(getUserInfoFsession().getId(), getIsAdmin(), tid, page, size);
        return "json";
    }

    @Action("getBlogContent")
    @ActionLog(desc = "获取博客内容")
    public String getBlogContent() {
        result = blogService.getBlogList(getUserInfoFsession().getId(), all == 1, getIsAdmin(), page, size);
        return "json";
    }

    @Action("getCommentList")
    @ActionLog(desc = "获取评论列表")
    public String getCommentList() {
        result = blogService.getCommentList(bid, page, size);
        return "json";
    }

    @Action("getTagList")
    @ActionLog(desc = "获取标签列表")
    public String getTagList() {
        result = blogService.getTagList(getUserInfoFsession().getId(), getIsAdmin());
        return "json";
    }

    @Action("getCateGoryList")
    @ActionLog(desc = "获取分类列表")
    public String getCateGoryList() {
        result = blogService.getCateGoryList(getUserInfoFsession().getId(), getIsAdmin());
        return "json";
    }

    /**
     * ---------------------------------添加----------------------------------------------------
     */

    @Action("addCategory")
    @ActionLog(desc = "增加分类")
    public String addCategory() {
        if (StringUtils.hasText(blogCategory.getName())
                && StringUtils.hasText(blogCategory.getColor())) {
            blogCategory.setUserId(getUserInfoFsession().getId());
            result = blogService.addCategory(blogCategory);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("addTag")
    @ActionLog(desc = "增加标签")
    public String addTag() {
        if (StringUtils.hasText(tags.getName())
                && StringUtils.hasText(tags.getColor())) {
            tags.setUserId(getUserInfoFsession().getId());
            result = blogService.addTag(tags);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("addBlog")
    @ActionLog(desc = "增加博客")
    public String addBlog() {
        if (StringUtils.hasText(blog.getOwnerCategoryUuid())
                && StringUtils.hasText(blog.getTitle())
                && StringUtils.hasText(blog.getIsmd() + "")
                && StringUtils.hasText(blog.getIsprivate() + "")
                && StringUtils.hasText(blog.getOwnerCategoryUuid())) {
            blog.setId(blogService.getBlogMax() + 1);
            blog.setOwnerUserId(getUserInfoFsession().getId());

            List<BlogTags> blogTagsList = new ArrayList<>();
            for (Tags tags : tagsList) {
                if (StringUtils.hasText(tags.getName())
                        && StringUtils.hasText(tags.getColor())) {
                    String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                    tags.setUserId(getUserInfoFsession().getId());
                    tags.setUuid(uuid);
                    BlogTags blogTags = new BlogTags();
                    blogTags.setBlogId(blog.getId());
                    blogTags.setTagsUuid(uuid);
                    blogTagsList.add(blogTags);
                }
            }
            result = blogService.addBlog(blog, tagsList, blogTagsList);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("addComment")
    @ActionLog(desc = "增加评论")
    public String addComment() {
        if (StringUtils.hasText(blogComment.getBlogId() + "")
                && StringUtils.hasText(blogComment.getCommentContent())
                && StringUtils.hasText(blogComment.getCommentUserId() + "")) {
            blogComment.setUserId(getUserInfoFsession().getId());
            result = blogService.addBlogComment(blogComment);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    /**
     * ---------------------------------修改----------------------------------------------------
     */

    @Action("updateCategory")
    @ActionLog(desc = "修改分类")
    public String updateCategory() {
        if (StringUtils.hasText(blogCategory.getName())
                && StringUtils.hasText(blogCategory.getColor())
                && StringUtils.hasText(blogCategory.getUuid())) {
            blogCategory.setUserId(getUserInfoFsession().getId());
            result = blogService.updateCategory(blogCategory);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("updateTag")
    @ActionLog(desc = "修改标签")
    public String updateTag() {
        if (StringUtils.hasText(tags.getName())
                && StringUtils.hasText(tags.getColor())
                && StringUtils.hasText(tags.getUuid())) {
            tags.setUserId(getUserInfoFsession().getId());
            result = blogService.updateTag(tags);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("updateBlog")
    @ActionLog(desc = "修改博客")
    public String updateBlog() {
        if (StringUtils.hasText(blog.getOwnerCategoryUuid())
                && StringUtils.hasText(blog.getTitle())
                && StringUtils.hasText(blog.getIsmd() + "")
                && StringUtils.hasText(blog.getIsprivate() + "")
                && StringUtils.hasText(blog.getId() + "")) {
            List<BlogTags> blogTagsList = new ArrayList<>();
            for (Tags tags : tagsList) {
                if (StringUtils.hasText(tags.getName())
                        && StringUtils.hasText(tags.getColor())
                        && StringUtils.hasText(tags.getUuid())) {
                    tags.setUserId(getUserInfoFsession().getId());
                    BlogTags blogTags = new BlogTags();
                    blogTags.setBlogId(blog.getId());
                    blogTags.setTagsUuid(tags.getUuid());
                    blogTagsList.add(blogTags);
                }
            }
            blog.setOwnerUserId(getUserInfoFsession().getId());

            result = blogService.updateBlog(blog, tagsList, blogTagsList);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("updateComment")
    @ActionLog(desc = "修改评论")
    public String updateComment() {
        if (StringUtils.hasText(blogComment.getUuid())) {
            result = blogService.updateBlogComment(blogComment);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    /**
     * ---------------------------------删除----------------------------------------------------
     */

    @Action("delCategory")
    @ActionLog(desc = "删除分类")
    public String delCategory() {
        if (StringUtils.hasText(blogCategory.getUuid())) {
            result = blogService.deleteCategory(blogCategory);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("delTag")
    @ActionLog(desc = "删除标签")
    public String delTag() {
        if (StringUtils.hasText(tags.getUuid())) {
            result = blogService.deleteTag(tags);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("delBlog")
    @ActionLog(desc = "删除博客")
    public String delBlog() {
        if (StringUtils.hasText(blog.getId() + "")) {
            result = blogService.deleteBlog(blog);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("delComment")
    @ActionLog(desc = "删除评论")
    public String delComment() {
        if (StringUtils.hasText(blogComment.getUuid())) {
            result = blogService.deleteBlogComment(blogComment);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }


    /**
     * -------------------------------------struts---------------------------------------------
     */

    private int all;
    private int page;
    private int size;
    private String cid;
    private String tid;
    private int bid;
    private Blog blog;
    private BlogComment blogComment;
    private BlogCategory blogCategory;
    private Tags tags;
    private BlogTags blogTags;
    private List<Tags> tagsList;

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public BlogComment getBlogComment() {
        return blogComment;
    }

    public void setBlogComment(BlogComment blogComment) {
        this.blogComment = blogComment;
    }

    public BlogCategory getBlogCategory() {
        return blogCategory;
    }

    public void setBlogCategory(BlogCategory blogCategory) {
        this.blogCategory = blogCategory;
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

    public BlogTags getBlogTags() {
        return blogTags;
    }

    public void setBlogTags(BlogTags blogTags) {
        this.blogTags = blogTags;
    }

    public List<Tags> getTagsList() {
        return tagsList;
    }

    public void setTagsList(List<Tags> tagsList) {
        this.tagsList = tagsList;
    }
}
