package blog.action;

import blog.aop.ActionLog;
import blog.common.utils.IntegerUtils;
import blog.common.utils.StaticUtils;
import blog.services.MailServices;
import org.apache.struts2.convention.annotation.*;
import org.springframework.context.annotation.Scope;
import blog.common.entity.UserInfo;
import blog.common.utils.SessionAction;
import blog.common.utils.ValidUtils;
import blog.services.UserService;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by HP on 16/12/31.
 */
@ParentPackage("base")
@Namespace("/user")
@Scope("prototype")
@Results({
        @Result(name = "json", type = "json", params = {"root", "result"}),
        @Result(name = "index", location = "/index.jsp")
})
public class UserAction extends SessionAction {

    private Object result;

    @Resource
    private
    UserService userService;

    @Resource
    private
    MailServices mailServices;

    @Action("reg")
    @ActionLog(desc = "注册")
    public String reg() {

        if (userInfo.getPassword().length() == 32
                && ValidUtils.match(3, userInfo.getEmail()) && ValidUtils.match(1, userInfo.getUsername())) {
            int identify = IntegerUtils.getIdentify();
            userInfo.setEmailIdentifyNum(identify);
            result = userService.reg(userInfo);
            //发送邮件到邮箱
            mailServices.sendEmail(userInfo.getEmail(), "久唯博客验证码", "验证码为" + identify);

            //存入session
            setIdentify(identify);
            putUserInfo2session(userInfo);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("login")
    @ActionLog(desc = "登录")
    public String login() {

        //可用 用户名，邮箱，uid 登录
        if ((ValidUtils.match(1, username) || ValidUtils.match(3, username) || ValidUtils.match(5, username))
//                &&StringUtils.match(2,password)
                && password.length() == 32
                ) {
            UserInfo userInfo = userService.login(username, password);
            if (userInfo != null) {
                putUserInfo2session(userInfo);
                result = StaticUtils.successMap;
            } else {
                result = StaticUtils.error_notFoundMap;
            }
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("logout")
    @ActionLog(desc = "退出登录")
    public String logout() {
        clearSession();
        result = StaticUtils.successMap;
        return "json";
    }

    @Action("mUn")
    @ActionLog(desc = "校验用户名")
    public String matchUserName() {
        if (ValidUtils.match(1, username)) {
            result = userService.matchUserName(username);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("mEm")
    @ActionLog(desc = "校验Email")
    public String matchEmail() {
        if (ValidUtils.match(3, email)) {
            result = userService.matchEmail(email);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("changePd")
    @ActionLog(desc = "修改密码")
    public String changePd() {
//        if (StringUtils.match(2,newPd)){
        if (password.length() == 32
                && newPd.length() == 32) {
            result = userService.changePd(username, password, newPd);
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

    @Action("sendEmail")
    @ActionLog(desc = "发送Email")
    public String sendEmail() {
        mailServices.sendEmail(userInfo.getEmail(), "久唯博客验证码", "验证码为" + IntegerUtils.getIdentify());
        result = StaticUtils.successMap;
        return "json";
    }

    @Action("fPd")
    @ActionLog(desc = "找回密码")
    public String fPd() {
        if (ValidUtils.match(3, email)) {
            Map map = userService.matchEmail(email);
            if (Boolean.parseBoolean(map.get("success").toString())) {
                //发送邮件
                mailServices.sendEmail(userInfo.getEmail(), "久唯博客验证码", "验证码为" + IntegerUtils.getIdentify());
                result = map;
            } else {
                result = StaticUtils.error_notFoundMap;
            }
        } else {
            result = StaticUtils.error_notValidMap;
        }
        return "json";
    }

//    ---------------------------------------------struts---------------------------------------------------------

    private UserInfo userInfo;
    private String username;
    private String password;
    private String email;

    private String newPd;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewPd() {
        return newPd;
    }

    public void setNewPd(String newPd) {
        this.newPd = newPd;
    }
}
