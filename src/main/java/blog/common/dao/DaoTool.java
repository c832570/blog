package blog.common.dao;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface DaoTool {
    public <T> void save(T t);

    <T> void onlySave(T t);

    public <T> void saveOrUpdate(T t);

    public <T> void saveLog(T t);

    public <T> void saveOrUpdateLog(T t);

    int findMax(String hql, String key, Object... params);

    public <T> void merge(T t);

    public <T> void delete(T t);

    public <T> void deleteLog(T t);

    public <T> void deleteById(Class<T> entityClass, String id);

    public void deleteObject(Object model);

    public <T> void update(T t);

    <T> void update(String sql);

    void updateOrDelete(String hql, Object... params);

    public <T, PK extends Serializable> T get(Class<T> entityClass, PK id);

    public <T> List<T> findAll(String hql, Class<T> entityClass);

    public <T> List<T> findAll(String hql, Class<T> entityClass, Object param);

    public <T> List<T> findAll(String hql, Class<T> entityClass, Object[] params);

    public <T> List<T> findByPage(final String hql, final Class<T> entityClass,
                                  final int firstResult, final int maxResult);

    public <T> List<T> findByPage(final String hql, final Class<T> entityClass,
                                  final Object param, final int firstResult, final int maxResult);

    public <T> List<T> findByPage(final String hql, final Class<T> entityClass,
                                  final Object[] params, final int firstResult, final int maxResult);

    public <T> Map<String, Object> findListAndCountByPage(final String hql, Class<T> entityClass,
                                                          final int firstResult, final int maxResult,
                                                          final String listName, final String countName);

    public <T> Map<String, Object> findListAndCountByPage(final String hql, Class<T> entityClass,
                                                          final Object param, final int firstResult, final int maxResult,
                                                          final String listName, final String countName);

    public <T> Map<String, Object> findListAndCountByPage(final String hql, Class<T> entityClass,
                                                          final Object[] params, final int firstResult, final int maxResult,
                                                          final String listName, final String countName);

    <T> Map<String, Object> findListAndCount(String hql, Class<T> entityClass,
                                             String listName, String countName);

    <T> Map<String, Object> findListAndCount(String hql, Class<T> entityClass,
                                             Object param, String listName, String countName);

    <T> Map<String, Object> findListAndCount(String hql, Class<T> entityClass,
                                             Object[] params, String listName, String countName);

    public int findCount(final String hql);

    public int findCount(final String hql, final Object param);

    public int findCount(final String hql, final Object[] params);

    public SQLQuery createSQLQuery(String sql);

    public Query createHQLQuery(String hql);

    public <T> List<T> findSQLListByPage(String sql, Class entityClass, int page, int pageSize, Object... params);

    public <T> List<T> findSQLListByPage(String hql, int page, int pageSize, Object... params);

    public int findSQLCount(String hql, Object... params);

    public Map<String,Object> findSqlCountAndList(String sql, Class entityClass, int page, int pageSize,
                                                  String listName, String countName, Object... params);

    public Map<String,Object> findSqlCountAndList(String hql, int page, int pageSize,
                                                  String listName, String countName, Object... params);

    public <T> List<T> findList(String hql, Object... params);

    public <T> List<T> findList(String hql, Class<T> entityClass, Object... params);

    public List findSQLList(String sql, Class entityClass, Object... params);

    public Session getCurrentSession();

    public List findSQLList(String sql, Boolean isChildMap, Object... params);

    public Object uniqueSQLResult(String sql, Boolean isChildMap, Object... params);
}
