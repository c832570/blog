package blog.common.dao.impl;

import org.hibernate.*;
import org.hibernate.transform.Transformers;
import blog.common.dao.DaoTool;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cuizhibin on 2017/3/7.
 */
public class DaoToolImpl implements DaoTool {

    private SessionFactory sessionFactory;


    public void deleteObject(Object model) {
        getSession().delete(model);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Session getSession() {
        return getCurrentSession();
//		return getOpenSession();
    }

    public void flush(){
        getSession().flush();
    }

    public <T> void save(T t) {
        getSession().save(t);
        flush();
    }

    public <T> void onlySave(T t) {
        getSession().save(t);
    }

    public <T> void saveOrUpdate(T t) {
        getSession().saveOrUpdate(t);
        flush();
    }

    public <T> void delete(T t) {
        getSession().delete(t);
        flush();
    }

    public <T> void deleteLog(T t) {
        getSession().delete(t);
    }

    public <T> void deleteById(Class<T> entityClass, String id) {
        getSession().delete(get(entityClass, id));
    }

    public <T> void saveLog(T t) {
        getSession().save(t);
    }

    public <T> void saveOrUpdateLog(T t) {
        getSession().saveOrUpdate(t);
    }


    public <T> void update(T t) {
        getSession().update(t);
        flush();
    }

    public <T> void update(String sql) {
        createSQLQuery(sql).executeUpdate();
        flush();
    }

    public void updateOrDelete(String hql, Object... params) {
        Query query = createHQLQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        query.executeUpdate();
        flush();
    }

    public <T, PK extends Serializable> T get(Class<T> entityClass, PK id) {
        return (T) getSession().get(entityClass, id);

    }

    public <T> List<T> findAll(String hql, Class<T> entityClass) {
        return findAll(hql, entityClass, new Object[] {});
    }


    public <T> List<T> findAll(String hql, Class<T> entityClass, Object param) {
        return findAll(hql, entityClass, new Object[] { param });
    }


    public <T> List<T> findAll(String hql, Class<T> entityClass, Object[] params) {
        Query query = getSession().createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return (List<T>) query.list();
    }


    public <T> List<T> findByPage(final String hql, Class<T> entityClass,
                                  final int firstResult, final int maxResult) {
        return findByPage(hql, entityClass, new Object[] {}, firstResult,
                maxResult);
    }


    public <T> List<T> findByPage(final String hql, Class<T> entityClass,
                                  final Object param, final int firstResult, final int maxResult) {
        return findByPage(hql, entityClass, new Object[] { param },
                firstResult, maxResult);
    }


    public <T> List<T> findByPage(final String hql, Class<T> entityClass,
                                  final Object[] params, final int firstResult, final int maxResult) {
        Query query = getSession().createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);
        return (List<T>) query.list();
    }

    public <T> Map<String, Object> findListAndCountByPage(final String hql, Class<T> entityClass,
                                  final int firstResult, final int maxResult,
                                                     final String listName, final String countName) {
        return findListAndCountByPage(hql, entityClass, new Object[] {}, firstResult, maxResult, listName, countName);
    }

    public <T> Map<String, Object> findListAndCountByPage(final String hql, Class<T> entityClass,
                                  final Object param, final int firstResult, final int maxResult,
                                                     final String listName, final String countName) {
        return findListAndCountByPage(hql, entityClass, new Object[] { param },
                firstResult, maxResult, listName, countName);
    }

    public <T> Map<String, Object> findListAndCountByPage(final String hql, Class<T> entityClass,
                                  final Object[] params, final int firstResult, final int maxResult,
                                                     final String listName, final String countName) {
        Map<String, Object> map = new HashMap<>();
        map.put(listName, findByPage(hql, entityClass, params, firstResult, maxResult));
        map.put(countName, findCount(hql, params));
        return map;
    }

    public <T> Map<String, Object> findListAndCount(final String hql, Class<T> entityClass,
                                                    final String listName, final String countName) {
        return findListAndCount(hql, entityClass, new Object[] {}, listName, countName);
    }

    public <T> Map<String, Object> findListAndCount(final String hql, Class<T> entityClass,
                                                    final Object param, final String listName, final String countName) {
        return findListAndCount(hql, entityClass, new Object[] { param },
                 listName, countName);
    }

    public <T> Map<String, Object> findListAndCount(final String hql, Class<T> entityClass,
                                  final Object[] params, final String listName, final String countName) {
        Map<String, Object> map = new HashMap<>();
        map.put(listName, findList(hql, entityClass, params));
        map.put(countName, findCount(hql, params));
        return map;
    }

    public int findCount(final String hql) {
        return findCount(hql, new Object[] {});
    }

    public int findCount(final String hql, final Object param) {
        return findCount(hql, new Object[] { param });
    }

    public int findCount(final String hql, final Object[] params) {
        Query query = getSession().createQuery("select count(1) " + hql.substring(hql.indexOf(" from ")));
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return ((Number)query.uniqueResult()).intValue();
    }

    public int findMax(final String hql, final String key, Object... params){
        Query query = getSession().createQuery("select max(" + key + ")" + hql.substring(hql.indexOf(" from ")));
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return ((Number)query.uniqueResult()).intValue();
    }

    public <T> void merge(T t) {
//		getSession().merge(t);
//		 //手动flush提交至数据库
//		getSession().flush();

        Session newSession = getOpenSession();
        newSession.merge(t);
        newSession.flush();
        newSession.close();
        newSession = null;
//		getOpenSession().merge(t);
//		 //手动flush提交至数据库
//		getOpenSession().flush();

    }

    public SQLQuery createSQLQuery(String sql) {
        return getSession().createSQLQuery(sql);
    }

    public Query createHQLQuery(String hql) {
        return getSession().createQuery(hql);
    }

    public <T> List<T> findList(String hql, Object... params) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        if(params!=null&&params.length>0){
            query = setQueryParams(query,params);
        }
        return query.list();
    }

    public <T> List<T> findList(String hql, Class<T> entityClass, Object... params) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        if(params!=null&&params.length>0){
            query = setQueryParams(query,params);
        }
        return (List<T>) query.list();
    }

    private Query setQueryParams(final Query query,final Object... params){
        for(int i=0;i<params.length;i++){
            Object obj = params[i];
            if (obj instanceof String) {
                String o = (String) obj;
                query.setString(i, o);
            } else if (obj instanceof Double) {
                Double o = (Double) obj;
                query.setDouble(i, o);
            } else if (obj instanceof Long) {
                Long o = (Long) obj;
                query.setLong(i,o);
            } else if (obj instanceof Integer) {
                Integer o = (Integer) obj;
                query.setInteger(i,o);
            } else if (obj instanceof Date) {
                Date o = (Date) obj;
                query.setDate(i,o);
            }else if (obj instanceof Short) {
                Short o = (Short) obj;
                query.setShort(i,o);
            } else {
                query.setString(i,obj==null?null:obj.toString());
            }
        }
        return query;
    }

    private SQLQuery setSqlQueryParams(final SQLQuery query,final Object... params){
        for(int i=0;i<params.length;i++){
            Object obj = params[i];
            if (obj instanceof String) {
                String o = (String) obj;
                query.setString(i, o);
            } else if (obj instanceof Double) {
                Double o = (Double) obj;
                query.setDouble(i, o);
            } else if (obj instanceof Long) {
                Long o = (Long) obj;
                query.setLong(i,o);
            } else if (obj instanceof Integer) {
                Integer o = (Integer) obj;
                query.setInteger(i,o);
            } else if (obj instanceof Date) {
                Date o = (Date) obj;
                query.setDate(i,o);
            }else if (obj instanceof Short) {
                Short o = (Short) obj;
                query.setShort(i,o);
            } else {
                query.setString(i,obj==null?null:obj.toString());
            }
        }
        return query;
    }

    public <T> List<T> findSQLListByPage(String sql, Class entityClass, int page, int pageSize, Object... params) {
        Query query = null;
        if(entityClass!=null){
            if(Map.class.isAssignableFrom(entityClass)){
                query = sessionFactory.getCurrentSession().createSQLQuery(sql);
                query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            }else{
                query = sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(entityClass);
            }
        }else{
            query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        }
        if(params!=null&&params.length>0){
            query = setQueryParams(query,params);
        }
        return query.setFirstResult((page-1)*pageSize).setMaxResults(pageSize).list();
    }

    public <T> List<T> findSQLListByPage(String hql, int page, int pageSize, Object... params) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        if(params!=null&&params.length>0){
            query = setQueryParams(query,params);
        }
        return query.setFirstResult((page-1)*pageSize).setMaxResults(pageSize).list();
    }

    public int findSQLCount(String sql, Object... params) {
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery("select count(1) " + sql.substring(sql.indexOf(" from ")));
        if(params!=null&&params.length>0){
            query = setSqlQueryParams(query,params);
        }
        return ((Number)query.uniqueResult()).intValue();
    }

    public Map<String,Object> findSqlCountAndList(String sql, Class entityClass, int page, int pageSize, String listName, String countName, Object... params){
        Map<String, Object> map = new HashMap<>();
        map.put(listName, findSQLListByPage(sql, entityClass, page, pageSize, params));
        map.put(countName, findSQLCount(sql, params));
        return map;
    }

    public Map<String,Object> findSqlCountAndList(String hql, int page, int pageSize, String listName, String countName, Object... params){
        Map<String, Object> map = new HashMap<>();
        map.put(listName, findSQLListByPage(hql, page, pageSize, params));
        map.put(countName, findSQLCount(hql, params));
        return map;
    }

    public List findSQLList(String sql, Class entityClass, Object... params) {
        Query query = null;
        if(entityClass!=null){
            if(Map.class.isAssignableFrom(entityClass)){
                query = sessionFactory.getCurrentSession().createSQLQuery(sql);
                query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            }else{
                query = sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(entityClass);
            }
        }else{
            query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        }
        if(params!=null&&params.length>0){
            query = setQueryParams(query,params);
        }
        return query.list();
    }

    public List findSQLList(String sql, Boolean isChildMap, Object... params) {
        SQLQuery sqlQuery = createSQLQuery(sql);
        Query query = setQueryParams(sqlQuery,params);
        if(isChildMap)query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return query.list();
    }

    public Object uniqueSQLResult(String sql, Boolean isChildMap, Object... params) {
        SQLQuery sqlQuery = createSQLQuery(sql);
        Query query = setQueryParams(sqlQuery,params);
        return query.uniqueResult();
    }

    public Session getCurrentSession(){
        return sessionFactory.getCurrentSession();
    }

    public Session getOpenSession(){
        return sessionFactory.openSession();
    }
}
