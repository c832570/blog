package blog.common.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cuizhibin on 2017/3/19.
 */
public class StaticUtils {
    public static boolean isInit = false;
    public static String basePath;

    public static final String LOGIN_USER_SESSION_KEY = "loginUser";
    public static final String LOGIN_USER_AUTH_KEY = "loginUserAuth";
    public static final String LOGIN_ADMIN_KEY = "admin";
    public static final String LOGIN_IDENTIFY_KEY = "identify";
    public static final String success = "操作成功";

    public static final String error_systemError = "系统错误";
    public static final String error_hasRecord = "记录已存在";
    public static final String error_notFound = "记录不存在";
    public static final String error_noAuth = "非法操作";
    public static final String error_notValid = "校验未通过，请检查输入";

    public static Map<String,Object> successMap = new HashMap<>();
    public static Map<String,Object> errorMap = new HashMap<>();
    public static Map<String,Object> error_hasRecordMap = new HashMap<>();
    public static Map<String,Object> error_notFoundMap = new HashMap<>();
    public static Map<String,Object> error_noAuthMap = new HashMap<>();
    public static Map<String,Object> error_notValidMap = new HashMap<>();

    public static void init(ApplicationContext applicationContext){
        try {
            WebApplicationContext webApplicationContext = (WebApplicationContext)applicationContext;
            ServletContext servletContext = webApplicationContext.getServletContext();
            basePath = servletContext.getRealPath("/");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    static {
        successMap.put("success", true);
        successMap.put("msg", success);

        errorMap.put("success", false);
        errorMap.put("msg", error_systemError);

        error_hasRecordMap.put("success", false);
        error_hasRecordMap.put("msg", error_hasRecord);

        error_notFoundMap.put("success", false);
        error_notFoundMap.put("msg", error_notFound);

        error_noAuthMap.put("success", false);
        error_noAuthMap.put("msg", error_noAuth);

        error_notValidMap.put("success", false);
        error_notValidMap.put("msg", error_notValid);
    }
}
