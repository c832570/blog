package blog.common.utils;

import com.opensymphony.xwork2.ActionContext;
import blog.common.entity.UserAuth;
import blog.common.entity.UserInfo;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by cuizhibin on 2017/3/18.
 */
public class SessionAction {

    public UserInfo getUserInfoFsession(){
        UserInfo userInfo = (UserInfo) ActionContext.getContext().getSession().get(StaticUtils.LOGIN_USER_SESSION_KEY);
        if (userInfo==null) {
            userInfo = new UserInfo();
            userInfo.setId(-1);
            return userInfo;
        }
        return userInfo;
    }

    public void putUserInfo2session(UserInfo userInfo){
        ActionContext.getContext().getSession().put(StaticUtils.LOGIN_USER_SESSION_KEY, userInfo);
    }

    public List<UserAuth> getUserAuthListFsession(){
        return (List<UserAuth>) ActionContext.getContext().getSession().get(StaticUtils.LOGIN_USER_AUTH_KEY);
    }

    public void putUserAuthList2session(List<UserAuth> userAuthList){
        ActionContext.getContext().getSession().put(StaticUtils.LOGIN_USER_AUTH_KEY, userAuthList);
    }

    public void clearSession(){
        ActionContext.getContext().getSession().clear();
    }

    public void setIsAdmin(){
        ActionContext.getContext().getSession().put(StaticUtils.LOGIN_ADMIN_KEY, false);
        List<UserAuth> userAuths = getUserAuthListFsession();
        if (userAuths.stream().anyMatch(userAuth -> userAuth.getRoleId() == 10000)){
            ActionContext.getContext().getSession().put(StaticUtils.LOGIN_ADMIN_KEY, true);
        }
    }

    public boolean getIsAdmin(){
        Object o = ActionContext.getContext().getSession().get(StaticUtils.LOGIN_ADMIN_KEY);
        return o != null && (boolean) o;
    }

    public void setIdentify(int identify) {
        ActionContext.getContext().getSession().put(StaticUtils.LOGIN_IDENTIFY_KEY, identify);
    }
    public int getIdentify() {
        return (int) ActionContext.getContext().getSession().get(StaticUtils.LOGIN_IDENTIFY_KEY);
    }
}
