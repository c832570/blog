package blog.common.utils;

import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by cuizhibin on 2017/3/7.
 */
public class ValidUtils {

    /**
     *
     * @param type      1用户名 2密码 3邮箱 4手机号 5uid
     * @param str       待验证字符串
     * @return
     */
    public static boolean match(int type, String str){
        if (StringUtils.isEmpty(str)) return false;
        String regex = "";
        switch (type){
            case 1:
                regex = "[A-Za-z0-9_\\-\\u4e00-\\u9fa5]{3,}";     //支持汉字字母数字下划线3到16位
                break;
            case 2:
                regex = "^(?![a-zA-z]+$)(?!\\d+$)(?![!@#$%^&*]+$)[a-zA-Z\\d!@#$%^&*]+$";    //必须包含字母数字特殊字符中的任意两种，特殊符号可以为!@#$%^&*
                break;
            case 3:
                regex = "\\w[-\\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14}";
                break;
            case 4:
                regex = "^(13[0-9]|14[5|7]|15[0-9]|17[0-9]|18[0-9])\\d{8}$";
                break;
            case 5:
                regex = "\\d";
                break;
            default:
                return false;
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str.trim());
        return matcher.matches();
    }
}
