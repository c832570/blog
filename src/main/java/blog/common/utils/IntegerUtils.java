package blog.common.utils;

import org.apache.commons.lang3.RandomUtils;

/**
 * Created by cuizhibin on 2017/4/4.
 */
public class IntegerUtils {

    public static int getIdentify() {
        return RandomUtils.nextInt(1000, 9999);
    }
}
