package blog.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by cuizhibin on 2017/4/3.
 */
public class FileUtils {
    /**
     * 将路径转为inputStream
     * @param path
     * @return
     */
    public static InputStream getStream(String path){
        try {
            File file = new File(path);
            if (file.exists() && file.isFile()){
                return new FileInputStream(file);
            }
        } catch (Exception ignored) {
        }
        return null;
    }
}
