package blog.common;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import blog.common.utils.StaticUtils;

/**
 * Created by cuizhibin on 2017/3/21.
 */
@Component
public class ApplicationListener implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext app) throws BeansException {
        applicationContext = app;
        if(!StaticUtils.isInit){
            StaticUtils.init(app);
            StaticUtils.isInit = true;
        }
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}

