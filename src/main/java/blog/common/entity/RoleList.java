package blog.common.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cuizhibin on 2017/3/18.
 */
@Entity
@Table(name = "role_list", schema = "blog", catalog = "")
public class RoleList {
    private int id;
    private String roleName;
    private Timestamp sysTime = new Timestamp(System.currentTimeMillis());
    private int state = 1;
    private String name;

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "role_name")
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Basic
    @Column(name = "sys_time")
    public Timestamp getSysTime() {
        return sysTime;
    }

    public void setSysTime(Timestamp sysTime) {
        this.sysTime = sysTime;
    }

    @Basic
    @Column(name = "state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleList roleList = (RoleList) o;

        if (id != roleList.id) return false;
        if (state != roleList.state) return false;
        if (roleName != null ? !roleName.equals(roleList.roleName) : roleList.roleName != null) return false;
        if (sysTime != null ? !sysTime.equals(roleList.sysTime) : roleList.sysTime != null) return false;
        if (name != null ? !name.equals(roleList.name) : roleList.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (roleName != null ? roleName.hashCode() : 0);
        result = 31 * result + (sysTime != null ? sysTime.hashCode() : 0);
        result = 31 * result + state;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
