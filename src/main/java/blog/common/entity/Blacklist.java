package blog.common.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cuizhibin on 2017/3/18.
 */
@Entity
public class Blacklist {
    private String uuid;
    private int userId;
    private int blackUserId;
    private Timestamp sysTime = new Timestamp(System.currentTimeMillis());
    private int state = 1;

    @Id
    @Column(name = "uuid")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Basic
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "black_user_id")
    public int getBlackUserId() {
        return blackUserId;
    }

    public void setBlackUserId(int blackUserId) {
        this.blackUserId = blackUserId;
    }

    @Basic
    @Column(name = "sys_time")
    public Timestamp getSysTime() {
        return sysTime;
    }

    public void setSysTime(Timestamp sysTime) {
        this.sysTime = sysTime;
    }

    @Basic
    @Column(name = "state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Blacklist blacklist = (Blacklist) o;

        if (userId != blacklist.userId) return false;
        if (blackUserId != blacklist.blackUserId) return false;
        if (state != blacklist.state) return false;
        if (uuid != null ? !uuid.equals(blacklist.uuid) : blacklist.uuid != null) return false;
        if (sysTime != null ? !sysTime.equals(blacklist.sysTime) : blacklist.sysTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + userId;
        result = 31 * result + blackUserId;
        result = 31 * result + (sysTime != null ? sysTime.hashCode() : 0);
        result = 31 * result + state;
        return result;
    }
}
