package blog.common.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cuizhibin on 2017/3/21.
 */
@Entity
@Table(name = "blog_comment", schema = "blog", catalog = "")
public class BlogComment {
    private String uuid;
    private int blogId;
    private int userId;
    private int isReplyUserRead = 0;
    private Timestamp sysTime = new Timestamp(System.currentTimeMillis());
    private String parentUuid;
    private int commentUserId;
    private String commentContent;

    @Id
    @Column(name = "uuid")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Basic
    @Column(name = "blog_id")
    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    @Basic
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "is_reply_user_read")
    public int getIsReplyUserRead() {
        return isReplyUserRead;
    }

    public void setIsReplyUserRead(int isReplyUserRead) {
        this.isReplyUserRead = isReplyUserRead;
    }

    @Basic
    @Column(name = "sys_time")
    public Timestamp getSysTime() {
        return sysTime;
    }

    public void setSysTime(Timestamp sysTime) {
        this.sysTime = sysTime;
    }

    @Basic
    @Column(name = "parent_uuid")
    public String getParentUuid() {
        return parentUuid;
    }

    public void setParentUuid(String parentUuid) {
        this.parentUuid = parentUuid;
    }

    @Basic
    @Column(name = "comment_user_id")
    public int getCommentUserId() {
        return commentUserId;
    }

    public void setCommentUserId(int commentUserId) {
        this.commentUserId = commentUserId;
    }

    @Basic
    @Column(name = "comment_content")
    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlogComment that = (BlogComment) o;

        if (blogId != that.blogId) return false;
        if (userId != that.userId) return false;
        if (isReplyUserRead != that.isReplyUserRead) return false;
        if (commentUserId != that.commentUserId) return false;
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;
        if (sysTime != null ? !sysTime.equals(that.sysTime) : that.sysTime != null) return false;
        if (parentUuid != null ? !parentUuid.equals(that.parentUuid) : that.parentUuid != null) return false;
        if (commentContent != null ? !commentContent.equals(that.commentContent) : that.commentContent != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + blogId;
        result = 31 * result + userId;
        result = 31 * result + isReplyUserRead;
        result = 31 * result + (sysTime != null ? sysTime.hashCode() : 0);
        result = 31 * result + (parentUuid != null ? parentUuid.hashCode() : 0);
        result = 31 * result + commentUserId;
        result = 31 * result + (commentContent != null ? commentContent.hashCode() : 0);
        return result;
    }
}
