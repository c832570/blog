package blog.common.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cuizhibin on 2017/5/6.
 */
@Entity
@Table(name = "user_info", schema = "blog", catalog = "")
public class UserInfo {
    private Integer id;
    private String username;
    private String password;
    private String email;
    private Integer isBlackList;
    private Integer state;
    private Timestamp regTime;
    private Integer emailValid;
    private Integer emailIdentifyNum;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "is_black_list")
    public Integer getIsBlackList() {
        return isBlackList;
    }

    public void setIsBlackList(Integer isBlackList) {
        this.isBlackList = isBlackList;
    }

    @Basic
    @Column(name = "state")
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Basic
    @Column(name = "reg_time")
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "email_valid")
    public Integer getEmailValid() {
        return emailValid;
    }

    public void setEmailValid(Integer emailValid) {
        this.emailValid = emailValid;
    }

    @Basic
    @Column(name = "email_identify_num")
    public Integer getEmailIdentifyNum() {
        return emailIdentifyNum;
    }

    public void setEmailIdentifyNum(Integer emailIdentifyNum) {
        this.emailIdentifyNum = emailIdentifyNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserInfo userInfo = (UserInfo) o;

        if (id != null ? !id.equals(userInfo.id) : userInfo.id != null) return false;
        if (username != null ? !username.equals(userInfo.username) : userInfo.username != null) return false;
        if (password != null ? !password.equals(userInfo.password) : userInfo.password != null) return false;
        if (email != null ? !email.equals(userInfo.email) : userInfo.email != null) return false;
        if (isBlackList != null ? !isBlackList.equals(userInfo.isBlackList) : userInfo.isBlackList != null) return false;
        if (state != null ? !state.equals(userInfo.state) : userInfo.state != null) return false;
        if (regTime != null ? !regTime.equals(userInfo.regTime) : userInfo.regTime != null) return false;
        if (emailValid != null ? !emailValid.equals(userInfo.emailValid) : userInfo.emailValid != null) return false;
        if (emailIdentifyNum != null ? !emailIdentifyNum.equals(userInfo.emailIdentifyNum) : userInfo.emailIdentifyNum != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (isBlackList != null ? isBlackList.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (regTime != null ? regTime.hashCode() : 0);
        result = 31 * result + (emailValid != null ? emailValid.hashCode() : 0);
        result = 31 * result + (emailIdentifyNum != null ? emailIdentifyNum.hashCode() : 0);
        return result;
    }
}
