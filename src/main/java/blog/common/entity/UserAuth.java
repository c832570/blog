package blog.common.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cuizhibin on 2017/3/18.
 */
@Entity
@Table(name = "user_auth", schema = "blog", catalog = "")
public class UserAuth {
    private String uuid;
    private int roleId;
    private int userId;
    private int state = 1;
    private Timestamp sysTime = new Timestamp(System.currentTimeMillis());
    private int executorId;
    private String executorName;

    @Id
    @Column(name = "uuid")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Basic
    @Column(name = "role_id")
    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Basic
    @Column(name = "sys_time")
    public Timestamp getSysTime() {
        return sysTime;
    }

    public void setSysTime(Timestamp sysTime) {
        this.sysTime = sysTime;
    }

    @Basic
    @Column(name = "executor_id")
    public int getExecutorId() {
        return executorId;
    }

    public void setExecutorId(int executorId) {
        this.executorId = executorId;
    }

    @Basic
    @Column(name = "executor_name")
    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAuth userAuth = (UserAuth) o;

        if (roleId != userAuth.roleId) return false;
        if (userId != userAuth.userId) return false;
        if (state != userAuth.state) return false;
        if (executorId != userAuth.executorId) return false;
        if (uuid != null ? !uuid.equals(userAuth.uuid) : userAuth.uuid != null) return false;
        if (sysTime != null ? !sysTime.equals(userAuth.sysTime) : userAuth.sysTime != null) return false;
        if (executorName != null ? !executorName.equals(userAuth.executorName) : userAuth.executorName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + roleId;
        result = 31 * result + userId;
        result = 31 * result + state;
        result = 31 * result + (sysTime != null ? sysTime.hashCode() : 0);
        result = 31 * result + executorId;
        result = 31 * result + (executorName != null ? executorName.hashCode() : 0);
        return result;
    }
}
