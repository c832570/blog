package blog.common.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cuizhibin on 2017/3/19.
 */
@Entity
public class Blog {
    private int id;
    private int ownerUserId;
    private int isprivate;
    private String title;
    private String content;
    private Timestamp sysTime = new Timestamp(System.currentTimeMillis());
    private int state = 1;
    private int ismd = 0;
    private String contentPreview;
    private String ownerCategoryUuid;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "owner_user_id")
    public int getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(int ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    @Basic
    @Column(name = "isprivate")
    public int getIsprivate() {
        return isprivate;
    }

    public void setIsprivate(int isprivate) {
        this.isprivate = isprivate;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "sys_time")
    public Timestamp getSysTime() {
        return sysTime;
    }

    public void setSysTime(Timestamp sysTime) {
        this.sysTime = sysTime;
    }

    @Basic
    @Column(name = "state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Basic
    @Column(name = "ismd")
    public int getIsmd() {
        return ismd;
    }

    public void setIsmd(int ismd) {
        this.ismd = ismd;
    }

    @Basic
    @Column(name = "content_preview")
    public String getContentPreview() {
        return contentPreview;
    }

    public void setContentPreview(String contentPreview) {
        this.contentPreview = contentPreview;
    }

    @Basic
    @Column(name = "owner_category_uuid")
    public String getOwnerCategoryUuid() {
        return ownerCategoryUuid;
    }

    public void setOwnerCategoryUuid(String ownerCategoryUuid) {
        this.ownerCategoryUuid = ownerCategoryUuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Blog blog = (Blog) o;

        if (id != blog.id) return false;
        if (ownerUserId != blog.ownerUserId) return false;
        if (isprivate != blog.isprivate) return false;
        if (state != blog.state) return false;
        if (ismd != blog.ismd) return false;
        if (title != null ? !title.equals(blog.title) : blog.title != null) return false;
        if (content != null ? !content.equals(blog.content) : blog.content != null) return false;
        if (sysTime != null ? !sysTime.equals(blog.sysTime) : blog.sysTime != null) return false;
        if (contentPreview != null ? !contentPreview.equals(blog.contentPreview) : blog.contentPreview != null) return false;
        if (ownerCategoryUuid != null ? !ownerCategoryUuid.equals(blog.ownerCategoryUuid) : blog.ownerCategoryUuid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + ownerUserId;
        result = 31 * result + isprivate;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (sysTime != null ? sysTime.hashCode() : 0);
        result = 31 * result + state;
        result = 31 * result + ismd;
        result = 31 * result + (contentPreview != null ? contentPreview.hashCode() : 0);
        result = 31 * result + (ownerCategoryUuid != null ? ownerCategoryUuid.hashCode() : 0);
        return result;
    }
}
