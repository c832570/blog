package blog.common.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cuizhibin on 2017/3/19.
 */
@Entity
@Table(name = "blog_tags", schema = "blog", catalog = "")
public class BlogTags {
    private String uuid;
    private Timestamp sysTime = new Timestamp(System.currentTimeMillis());
    private String tagsUuid;
    private int blogId;

    @Id
    @Column(name = "uuid")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Basic
    @Column(name = "sys_time")
    public Timestamp getSysTime() {
        return sysTime;
    }

    public void setSysTime(Timestamp sysTime) {
        this.sysTime = sysTime;
    }

    @Basic
    @Column(name = "tags_uuid")
    public String getTagsUuid() {
        return tagsUuid;
    }

    public void setTagsUuid(String tagsUuid) {
        this.tagsUuid = tagsUuid;
    }

    @Basic
    @Column(name = "blog_id")
    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlogTags blogTags = (BlogTags) o;

        if (blogId != blogTags.blogId) return false;
        if (uuid != null ? !uuid.equals(blogTags.uuid) : blogTags.uuid != null) return false;
        if (sysTime != null ? !sysTime.equals(blogTags.sysTime) : blogTags.sysTime != null) return false;
        if (tagsUuid != null ? !tagsUuid.equals(blogTags.tagsUuid) : blogTags.tagsUuid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + (sysTime != null ? sysTime.hashCode() : 0);
        result = 31 * result + (tagsUuid != null ? tagsUuid.hashCode() : 0);
        result = 31 * result + blogId;
        return result;
    }
}
