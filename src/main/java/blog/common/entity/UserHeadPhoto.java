package blog.common.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cuizhibin on 2017/3/18.
 */
@Entity
@Table(name = "user_head_photo", schema = "blog", catalog = "")
public class UserHeadPhoto {
    private String uuid;
    private int userId;
    private String picPath;
    private int picState = 1;
    private Timestamp sysTime = new Timestamp(System.currentTimeMillis());

    @Id
    @Column(name = "uuid")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Basic
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "pic_path")
    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    @Basic
    @Column(name = "pic_state")
    public int getPicState() {
        return picState;
    }

    public void setPicState(int picState) {
        this.picState = picState;
    }

    @Basic
    @Column(name = "sys_time")
    public Timestamp getSysTime() {
        return sysTime;
    }

    public void setSysTime(Timestamp sysTime) {
        this.sysTime = sysTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserHeadPhoto that = (UserHeadPhoto) o;

        if (userId != that.userId) return false;
        if (picState != that.picState) return false;
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;
        if (picPath != null ? !picPath.equals(that.picPath) : that.picPath != null) return false;
        if (sysTime != null ? !sysTime.equals(that.sysTime) : that.sysTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + userId;
        result = 31 * result + (picPath != null ? picPath.hashCode() : 0);
        result = 31 * result + picState;
        result = 31 * result + (sysTime != null ? sysTime.hashCode() : 0);
        return result;
    }
}
