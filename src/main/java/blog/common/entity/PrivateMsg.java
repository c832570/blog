package blog.common.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cuizhibin on 2017/3/21.
 */
@Entity
@Table(name = "private_msg", schema = "blog", catalog = "")
public class PrivateMsg {
    private String uuid;
    private int userId;
    private int toUserId;
    private String content;
    private Timestamp sysTime = new Timestamp(System.currentTimeMillis());
    private int state = 1;
    private int isSysMsg = 0;

    @Id
    @Column(name = "uuid")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Basic
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "to_user_id")
    public int getToUserId() {
        return toUserId;
    }

    public void setToUserId(int toUserId) {
        this.toUserId = toUserId;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "sys_time")
    public Timestamp getSysTime() {
        return sysTime;
    }

    public void setSysTime(Timestamp sysTime) {
        this.sysTime = sysTime;
    }

    @Basic
    @Column(name = "state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Basic
    @Column(name = "is_sys_msg")
    public int getIsSysMsg() {
        return isSysMsg;
    }

    public void setIsSysMsg(int isSysMsg) {
        this.isSysMsg = isSysMsg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrivateMsg that = (PrivateMsg) o;

        if (userId != that.userId) return false;
        if (toUserId != that.toUserId) return false;
        if (state != that.state) return false;
        if (isSysMsg != that.isSysMsg) return false;
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (sysTime != null ? !sysTime.equals(that.sysTime) : that.sysTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + userId;
        result = 31 * result + toUserId;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (sysTime != null ? sysTime.hashCode() : 0);
        result = 31 * result + state;
        result = 31 * result + isSysMsg;
        return result;
    }
}
