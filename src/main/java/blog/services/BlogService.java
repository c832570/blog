package blog.services;

import blog.common.entity.*;

import java.util.List;
import java.util.Map;

/**
 * Created by cuizhibin on 2017/3/18.
 */
public interface BlogService {

    Map<String, Object> getBlogList(int uid, boolean isAll, boolean isAdmin, int page, int size);

    int getBlogMax();

    Map<String, Object> getBlogListByCategory(int uid, boolean isAdmin, String cid, int page, int size);

    Map<String, Object> getBlogListByTag(int uid, boolean isAdmin, String tid, int page, int size);

    Map<String, Object> getTagList(int uid, boolean isAdmin);

    Map<String, Object> getCateGoryList(int uid, boolean isAdmin);

    Map<String, Object> getBlogContent(int bid, int uId, boolean isAdmin);

    Map<String, Object> getCommentList(int bid, int page, int size);

    Map<String,Object> addCategory(BlogCategory category);

    Map<String,Object> updateCategory(BlogCategory category);

    Map<String,Object> deleteCategory(BlogCategory category);

    Map<String,Object> addTag(Tags tag);

    Map<String,Object> updateTag(Tags tag);

    Map<String,Object> deleteTag(Tags tag);

    Map<String,Object> addBlog(Blog blog, List<Tags> tagsList, List<BlogTags> blogTagsList);

    Map<String,Object> updateBlog(Blog blog, List<Tags> tagsList, List<BlogTags> blogTagsList);

    Map<String,Object> deleteBlog(Blog blog);

    Map<String,Object> addBlogComment(BlogComment blogComment);

    Map<String,Object> updateBlogComment(BlogComment blogComment);

    Map<String,Object> deleteBlogComment(BlogComment blogComment);
}
