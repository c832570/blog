package blog.services;

import blog.common.entity.UserInfo;

import java.util.Map;

/**
 * Created by cuizhibin on 2017/3/7.
 */
public interface UserService {
    Map<String, Object> reg(UserInfo userInfo);

    UserInfo login(String userName, String password);

    Map<String, Object> matchUserName(String userName);

    Map<String, Object> matchEmail(String email);

    Map<String, Object> changePd(String userName, String password, String newPd);
}
