package blog.services;

/**
 * Created by cuizhibin on 2017/5/6.
 */
public interface MailServices {

    void sendEmail(String receiver, String subject, String text);
}
