package blog.services.impl;

import blog.common.dao.DaoTool;
import blog.common.entity.UserHeadPhoto;
import blog.common.utils.FileUtils;
import blog.common.utils.StaticUtils;
import blog.services.PicServices;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * Created by cuizhibin on 2017/4/3.
 */
@Service("picServices")
public class PicServicesImpl implements PicServices {

    @Resource
    DaoTool daoTool;

    @Override
    public InputStream getHeadPhoto(int uid){
        List<UserHeadPhoto> list = daoTool.findList("from UserHeadPhoto where userId=? and picState=1 ", UserHeadPhoto.class, uid);
        if (list.size()>0){
            return FileUtils.getStream(list.get(0).getPicPath());
        }
        return FileUtils.getStream(StaticUtils.basePath + "common\\images\\avatar.png");
    }
}
