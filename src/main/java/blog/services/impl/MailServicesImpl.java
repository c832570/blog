package blog.services.impl;

import blog.services.MailServices;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by cuizhibin on 2017/5/6.
 */
@Service("mailServices")
public class MailServicesImpl implements MailServices {

    @Resource
    private JavaMailSenderImpl sender;

    /**
     * 发送邮件
     * @param receiver  接收地址
     * @param subject   主题
     * @param text      文本内容
     */
    @Override
    public void sendEmail(String receiver, String subject, String text) {
        SimpleMailMessage smm = new SimpleMailMessage();
        // 设定邮件参数
        smm.setFrom(sender.getUsername());
        smm.setTo("776005958@qq.com");
        smm.setSubject("Hello world");
        smm.setText("Hello world via spring mail sender");

        sender.send(smm);
    }
}
