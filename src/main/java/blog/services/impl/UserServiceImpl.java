package blog.services.impl;

import blog.common.utils.StaticUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import blog.common.dao.DaoTool;
import blog.common.entity.UserInfo;
import blog.services.UserService;

import javax.annotation.Resource;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cuizhibin on 2017/3/7.
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Resource
    private
    DaoTool daoTool;

    /**
     * 注册
     *
     * @param userInfo
     * @return
     */
    @Override
    public Map<String, Object> reg(UserInfo userInfo) {
        daoTool.save(userInfo);
        return StaticUtils.successMap;
    }

    /**
     * 登录
     *
     * @param userName
     * @param password
     * @return
     */
    @Override
    public UserInfo login(String userName, String password) {
        List<UserInfo> list = daoTool.findList("from UserInfo where (username=? or email=? or id=?) and password=?", userName, userName, NumberUtils.toInt(userName, -1), password);
        if (list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    /**
     * 验证用户名是否存在
     *
     * @param userName
     * @return
     */
    @Override
    public Map<String, Object> matchUserName(String userName) {
        List<UserInfo> list = daoTool.findList("from UserInfo where username=? ", userName);
        if (list.size() > 0) {
            return StaticUtils.error_hasRecordMap;
        }
        return StaticUtils.successMap;
    }

    /**
     * 验证邮箱是否存在
     *
     * @param email
     * @return
     */
    @Override
    public Map<String, Object> matchEmail(String email) {
        List<UserInfo> list = daoTool.findList("from UserInfo where email=? ", email);
        if (list.size() > 0) {
            return StaticUtils.error_hasRecordMap;
        }
        return StaticUtils.successMap;
    }

    /**
     * 改密
     *
     * @param userName
     * @param password
     * @param newPd
     * @return
     */
    @Override
    public Map<String, Object> changePd(String userName, String password, String newPd) {
        List<UserInfo> list = daoTool.findList("from UserInfo where userName=? and password=? ", userName, password);
        if (list.size() > 0) {
            UserInfo userInfo = list.get(0);
            userInfo.setPassword(newPd);
            daoTool.update(userInfo);
            return StaticUtils.successMap;
        } else {
            return StaticUtils.error_notFoundMap;
        }
    }
}
