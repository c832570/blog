package blog.services.impl;

import org.springframework.stereotype.Service;
import blog.common.dao.DaoTool;
import blog.common.entity.*;
import blog.common.utils.StaticUtils;
import blog.services.BlogService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cuizhibin on 2017/3/18.
 */
@Service("blogService")
public class BlogServiceImpl implements BlogService {

    @Resource
    private
    DaoTool daoTool;

    /**
     * 查询所有
     *
     * @param uid
     * @param isAll
     * @param page
     * @param size
     * @return
     */
    @Override
    public Map<String, Object> getBlogList(int uid, boolean isAll, boolean isAdmin, int page, int size) {
        Map<String, Object> returnMap = new HashMap<>();

        if (isAdmin) {
            returnMap = daoTool.findSqlCountAndList("select * from blog_list_view", Map.class, page, size, "rows", "total");
        } else if (isAll) {
            returnMap = daoTool.findSqlCountAndList("select * from blog_list_view where owner_user_id=? or isprivate = 0", Map.class, page, size, "rows", "total", uid);
        } else {
            returnMap = daoTool.findSqlCountAndList("select * from blog_list_view where owner_user_id=?", Map.class, page, size, "rows", "total", uid);
        }
        returnMap.put("success", true);
        return returnMap;
    }

    /**
     * 获取博客最大ID
     * @return
     */
    @Override
    public int getBlogMax(){
        return daoTool.findMax("from Blog", "id");
    }
    /**
     * 分类查询
     *
     * @param uid
     * @param cid
     * @param page
     * @param size
     * @return
     */
    @Override
    public Map<String, Object> getBlogListByCategory(int uid, boolean isAdmin, String cid, int page, int size) {
        Map<String, Object> returnMap = new HashMap<>();
        if (isAdmin) {
            returnMap = daoTool.findSqlCountAndList("select * from blog_list_view where owner_category_uuid = ?", Map.class, page, size, "rows", "total", cid);
        } else {
            returnMap = daoTool.findSqlCountAndList("select * from blog_list_view where owner_user_id=? and owner_category_uuid = ?", Map.class, page, size, "rows", "total", uid, cid);
        }

        return returnMap;
    }

    /**
     * 根据标签查询
     * @param uid
     * @param tid
     * @param page
     * @param size
     * @return
     */
    @Override
    public Map<String, Object> getBlogListByTag(int uid, boolean isAdmin, String tid, int page, int size) {
        Map<String, Object> returnMap = new HashMap<>();
        if (isAdmin) {
            returnMap = daoTool.findSqlCountAndList("select * from blog_list_view v, blog_tags t where v.id=t.blog_id and t.tags_uuid = ?", Map.class, page, size, "rows", "total", tid);
        } else {
            returnMap = daoTool.findSqlCountAndList("select * from blog_list_view v, blog_tags t where v.id=t.blog_id and t.tags_uuid = ? and v.owner_user_id=?", Map.class, page, size, "rows", "total", tid, uid);
        }
        returnMap.put("success", true);
        return returnMap;
    }

    /**
     * 查询该用户所有标签
     * @param uid
     * @return
     */
    @Override
    public Map<String, Object> getTagList(int uid, boolean isAdmin) {
        Map<String, Object> returnMap = new HashMap<>();
        if (isAdmin) {
            returnMap = daoTool.findListAndCount("from Tags", Map.class, "rows", "total");
        } else {
            returnMap = daoTool.findListAndCount("from Tags t where t.user_id = ?", Map.class, uid, "rows", "total");
        }
        returnMap.put("success", true);
        return returnMap;
    }

    /**
     * 查询该用户所有分类
     * @param uid
     * @return
     */
    @Override
    public Map<String, Object> getCateGoryList(int uid, boolean isAdmin) {
        Map<String, Object> returnMap = new HashMap<>();

        if (isAdmin) {
            returnMap = daoTool.findListAndCount("from BlogCategory", Map.class, "rows", "total");
        } else {
            returnMap = daoTool.findListAndCount("from BlogCategory c where c.user_id = ?", Map.class, uid, "rows", "total");
        }
        returnMap.put("success", true);
        return returnMap;
    }

    /**
     * 获取博客内容
     * @param bid
     * @param uId
     * @param isAdmin
     * @return
     */
    @Override
    public Map<String, Object> getBlogContent(int bid, int uId, boolean isAdmin) {
        Map<String, Object> returnMap = new HashMap<>();

        if (isAdmin) {
            returnMap.put("blog", daoTool.findList("from Blog where id=?", bid));
        } else {
            returnMap.put("blog", daoTool.findList("from Blog where id=? and (owner_user_id=? or isprivate = 0)", bid, uId));
        }
        returnMap.put("success", true);
        return returnMap;
    }

    /**
     * 获取博客评论
     * @param page
     * @param size
     * @return
     */
    @Override
    public Map<String, Object> getCommentList(int bid, int page, int size) {
        Map<String, Object> returnMap = daoTool.findListAndCountByPage("from BlogComment where blogId=?", Blog.class, bid, page, size, "rows", "total");
        returnMap.put("success", true);
        return returnMap;
    }

    /**
     * 增加分类
     * @param category
     * @return
     */
    @Override
    public Map<String,Object> addCategory(BlogCategory category){
        int count = daoTool.findCount("from BlogCategory where name=? and userId=? and parentUuid=?", new Object[]{category.getName(), category.getUserId(), category.getParentUuid()});
        if (count>0){

            return StaticUtils.error_hasRecordMap;
        }
        daoTool.save(category);
        return StaticUtils.successMap;
    }

    /**
     * 修改分类
     * @param category
     * @return
     */
    @Override
    public Map<String,Object> updateCategory(BlogCategory category){
        int count = daoTool.findCount("from BlogCategory where name=? and userId=? and parentUuid=?", new Object[]{category.getName(), category.getUserId(), category.getParentUuid()});
        if (count<0){
            return StaticUtils.error_notFoundMap;
        }
        daoTool.update(category);
        return StaticUtils.successMap;
    }

    /**
     * 删除分类
     * @param category
     * @return
     */
    @Override
    public Map<String,Object> deleteCategory(BlogCategory category){
        int count = daoTool.findCount("from BlogCategory where name=? and userId=? and parentUuid=?", new Object[]{category.getName(), category.getUserId(), category.getParentUuid()});
        if (count<0){
            return StaticUtils.error_notFoundMap;
        }
        daoTool.updateOrDelete("update Blog set ownerCategoryUuid=? where ownerCategoryUuid=? or ownerCategoryUuid=(select uuid from BlogCategory where parentUuid=?)",category.getParentUuid(), category.getUuid());
        daoTool.updateOrDelete("delete BlogCategory where uuid=? or parentUuid=?",category.getUuid(),category.getUuid());
        return StaticUtils.successMap;
    }

    /**
     * 增加标签
     * @param tag
     * @return
     */
    @Override
    public Map<String,Object> addTag(Tags tag){
        int count = daoTool.findCount("from Tags where name=? and userId=?", new Object[]{tag.getName(), tag.getUserId()});
        if (count>0){
            return StaticUtils.error_hasRecordMap;
        }
        daoTool.save(tag);
        return StaticUtils.successMap;
    }

    /**
     * 修改标签
     * @param tag
     * @return
     */
    @Override
    public Map<String,Object> updateTag(Tags tag){
        int count = daoTool.findCount("from Tags where name=? and userId=?", new Object[]{tag.getName(), tag.getUserId()});
        if (count<0){
            return StaticUtils.error_notFoundMap;
        }
        daoTool.update(tag);
        return StaticUtils.successMap;
    }

    /**
     * 删除标签
     * @param tag
     * @return
     */
    @Override
    public Map<String,Object> deleteTag(Tags tag){
        int count = daoTool.findCount("from Tags where name=? and userId=? and parentUuid=?", new Object[]{tag.getName(), tag.getUserId()});
        if (count<0){
            return StaticUtils.error_notFoundMap;
        }
        daoTool.updateOrDelete("delete BlogTags where tagsUuid=?","", tag.getUuid());
        daoTool.delete(tag);
        return StaticUtils.successMap;
    }

    /**
     * 增加博客
     * @param blog
     * @return
     */
    @Override
    public Map<String,Object> addBlog(Blog blog, List<Tags> tagsList, List<BlogTags> blogTagsList){
        daoTool.save(blog);
        for (Tags tags : tagsList) {
            daoTool.saveOrUpdate(tags);
        }
        for (BlogTags tags : blogTagsList){
            daoTool.onlySave(tags);
        }
        daoTool.getCurrentSession().flush();
        daoTool.getCurrentSession().clear();
        return StaticUtils.successMap;
    }

    /**
     * 修改博客
     * //TODO 加历史记录
     * @param blog
     * @return
     */
    @Override
    public Map<String,Object> updateBlog(Blog blog, List<Tags> tagsList, List<BlogTags> blogTagsList){
        int count = daoTool.findCount("from Blog where id=?", new Object[]{blog.getId()});
        if (count<0){
            return StaticUtils.error_notFoundMap;
        }
        daoTool.update(blog);
        daoTool.updateOrDelete("delete BlogTags where blog_id=?",blog.getId());
        for (Tags tags : tagsList) {
            daoTool.saveOrUpdate(tags);
        }
        for (BlogTags tags : blogTagsList){
            daoTool.onlySave(tags);
        }
        daoTool.getCurrentSession().flush();
        daoTool.getCurrentSession().clear();
        return StaticUtils.successMap;
    }

    /**
     * 删除博客
     * @param blog
     * @return
     */
    @Override
    public Map<String,Object> deleteBlog(Blog blog){
        int count = daoTool.findCount("from Blog where id=?", blog.getId());
        if (count<0){
            return StaticUtils.error_notFoundMap;
        }
        daoTool.delete(blog);
        daoTool.updateOrDelete("delete BlogTags where blogId=?",blog.getId());
        return StaticUtils.successMap;
    }

    /**
     * 增加博客评论
     * @param blogComment
     * @return
     */
    @Override
    public Map<String,Object> addBlogComment(BlogComment blogComment){
        daoTool.save(blogComment);
        daoTool.update("update Blog_comment set is_reply_user_read=1 where uuid='" + blogComment.getUuid() + "'");
        return StaticUtils.successMap;
    }

    /**
     * 修改博客评论状态
     * @return
     */
    @Override
    public Map<String,Object> updateBlogComment(BlogComment blogComment){
        int count = daoTool.findCount("from BlogComment where uuid=?", blogComment.getUuid());
        if (count<0){
            return StaticUtils.error_notFoundMap;
        }
        daoTool.update("update Blog_comment set is_reply_user_read=1");
        return StaticUtils.successMap;
    }

    /**
     * 删除博客评论
     * @param blogComment
     * @return
     */
    @Override
    public Map<String,Object> deleteBlogComment(BlogComment blogComment){
        int count = daoTool.findCount("from BlogComment where uuid=?", blogComment.getUuid());
        if (count<0){
            return StaticUtils.error_notFoundMap;
        }
        daoTool.updateOrDelete("delete BlogComment where uuid=? or parent_uuid=?",blogComment.getUuid(),blogComment.getUuid());
        return StaticUtils.successMap;
    }

}

