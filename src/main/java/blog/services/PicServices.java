package blog.services;

import java.io.InputStream;

/**
 * Created by cuizhibin on 2017/4/3.
 */
public interface PicServices {
    InputStream getHeadPhoto(int uid);
}
